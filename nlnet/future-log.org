#+TITLE: openEngiadina: Futurelog

Ideas that we keep for the future.

* Backend
** Capability-based security
   :PROPERTIES:
   :Lead:     pukkamustard
   :EffortE:  medium
   :Deliverable: proof-of-concept
   :END:

- [Ocap](https://gitlab.com

* Research: Semantic Client
** Interface for creating Semantic Content
   :PROPERTIES:
   :Lead:     Akshay
   :effortE:  medium
   :END:
** Automatic structured content creation
   :PROPERTIES:
   :Lead:     Akshay
   :EffortE:  high
   :END:
** Semantic search and filter
   :PROPERTIES:
   :Lead:     Akshay
   :EffortE:  medium
   :END:
** Natural language queries to structured queries   
   :PROPERTIES:
   :EffortE:  high
   :Lead:     Akshay
   :END:
* Research: Decentralization
** Theory of Patches for Linked Data
   :PROPERTIES:
   :Lead:     pukkamustard
   :EffortE:  medium
   :Deliverable: proof-of-concept
   :END:
** GNUNet
   :PROPERTIES:
   :Lead:     pukkamustard
   :EffortE:  medium
   :Deliverable: proof-of-concept
   :END:

  - How can ideas from the ActivityPub Semantic Social Network be applied to the rescilient and distributed GNUNet?
  - Research into the [secushare protocol](https://secushare.org/protocol)

** IPFS / IPLD
   :PROPERTIES:
   :Lead:     Akshay
   :EffortE:  medium
   :END:

  - Publish structured data to IPFS
  - Search and query structured data on IPFS
  
** DID & Verifiable credentials
   :PROPERTIES:
   :Lead:     Akshay
   :EffortE:  low
   :END:

- [DID](https://w3c-ccg.github.io/did-spec/)
- [Verifiable credentials](https://www.w3.org/TR/vc-data-model/)
- 
** Research: Linked Data and the Semantic Web
* Serialization of Linked Data
* Creating and publishing vocabularies
  :PROPERTIES:
  :Lead:     pukkamustard
  :EffortE:  high
  :Deliverable: blog
  :Deliverable+: proof-of-concept
  :END:

A major challenge when using Semantic Web technology is agreeing upon a vocabulary to describe the data.

- Research existing methods for creating and publishing vocabularies
  - [[https://protege.stanford.edu/][Protégé]]
- Develop prototype of a domain specific language to describe ontologies/vocablulary

* Example: A distributed GoodReads
  :PROPERTIES:
  :Lead:     Akshay
  :EffortE:  high
  :END:
